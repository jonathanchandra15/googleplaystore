from django.shortcuts import render
from SPARQLWrapper import SPARQLWrapper, JSON
from .forms import SearchForm

ALLOWED_KEYS = ['name', 'category', 'type', 'genre']
CAPITALIZED = ['name', 'type', 'genre']
ALL_CAPS = ['category']

def home(request):
    form = SearchForm()
    return render(request, 'homepage.html', {'form': form})

def search(request):
    if request.method == 'POST':
        # query_str = request.POST.get('query', '')
        query_str = SearchForm(request.POST)['search'].value()
        if query_str != '':
            query_keypair = parseQuery(query_str)
        else:
            query_keypair = {}
    else:
        query_str = ''
        query_keypair = {}

    result = queryGraph(query_keypair)
    if(len(result['results']['bindings']) == 0):
      result = 0

    return render(request, 'search.html', {'result' : result, 'query_str' : query_str})

def queryGraph(query_keypair):
    sparql = SPARQLWrapper("http://localhost:9999/blazegraph/sparql")
    sparql.setQuery(buildQuery(query_keypair))
    sparql.setReturnFormat(JSON)
    return sparql.query().convert()

def buildQuery(query_keypair):
    filter = ''
    for key in query_keypair.keys():
        filter = filter + 'filter(regex(?%s, "%s", "i")) .' % (key, query_keypair[key])
    if filter != '': 
        filter = filter[:-1] + '.'
    
    sparql_query = '''
    prefix ns1: <http://googleplaystore.org/>

    SELECT ?app ?id ?name ?category ?type ?genre
    WHERE {
        ?app ns1:hasId ?id ;
             ns1:hasAppName ?name ;
             ns1:hasCategory ?category ;
             ns1:hasType ?type ;
             ns1:hasGenre ?genre .
        %s
    }
    ''' % filter
    
    print(sparql_query)

    return sparql_query

def parseQuery(query_str):
    query_keypair = {}

    keypairs = query_str.split(';')
    for keypair in keypairs:
        split_keypair = keypair.split(':')
        key = split_keypair[0]
        value = ' '.join(split_keypair[1:])
        if key not in ALLOWED_KEYS:
            continue
        
        query_keypair[key] = formatValue(key, value)
    
    return query_keypair

def formatValue(key, value):
    if key in CAPITALIZED:
        return value.capitalize()
    if key in ALL_CAPS:
        return value.upper()
    return value
