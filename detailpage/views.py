from qwikidata.sparql import (get_subclasses_of_item,
                              return_sparql_query_results)
from qwikidata.entity import WikidataItem, WikidataLexeme, WikidataProperty
from qwikidata.linked_data_interface import get_entity_dict_from_api
from django.shortcuts import render
from SPARQLWrapper import SPARQLWrapper, JSON

WD_PROPS = [
    'wdt:P571', # Inception time
    'wdt:P1705', # Native label
    'wdt:P112', # Founder(s)
    'wdt:P17', # Country
    'wdt:P159', # Headquarters Location
    'wdt:P123', # Publisher
    'wdt:P750', # Distributor(s)
    'wdt:P137', # Operator
    'wdt:P178', # Developer
    'wdt:P400', # Platform(s)
    'wdt:P277', # Programming Language(s)
    'wdt:P1324', # Source code URL
    'wdt:P348', # Software version
    'p:P154', 
]

def get_detail(request, id):
    result = query_details(id)
    wikidata = result[1]
    if len(result[1]) == 0:
      wikidata = 0
    return render(request, 'detailpage.html', {'local': result[0], 'wikidata': wikidata, 'name': result[0]['AppName'][0]})

def queryGraph(query_str):
    sparql = SPARQLWrapper("http://localhost:9999/blazegraph/sparql")
    sparql.setQuery(query_str)
    sparql.setReturnFormat(JSON)
    return sparql.query().convert()

def query_details_local(id):
    sparql_query = '''
    prefix ns1: <http://googleplaystore.org/app/>

    SELECT ?prop ?obj
    WHERE {
        ns1:%s ?prop ?obj.
    }
    ''' % id

    res = queryGraph(sparql_query)
    claims = {}
    for claim in res['results']['bindings']:
        prop = claim['prop']['value'].split('/')[-1][3:]
        obj = claim['obj']['value']
        if prop not in claims:
            claims[prop] = []
        claims[prop].append(obj)
    return claims

def get_interesting_props():
    tmp = ','.join(WD_PROPS)
    return f'({tmp})'

def query_details_wikidata(query_name):
    wd_query = (
        'SELECT DISTINCT ?propLabel ?objLabel ' +
        'WHERE ' +
        '{ ' +
        '{ ?app wdt:P31 wd:Q620615. }' +
        'UNION' +
        '{ ?app wdt:P306 wd:Q94. }' +
        ' ' +
        '?app rdfs:label ?appLabel. ' +
        'FILTER(LANG(?appLabel) = "en"). ' +
        f'FILTER(CONTAINS(?appLabel, "{query_name}")). ' +
        ' ' +
        '?app ?prop ?obj. ' +
        '?claim wikibase:directClaim ?prop. ' +
        '?claim rdfs:label ?propLabel. ' +
        'FILTER(LANG(?propLabel) = "en"). ' +
        f'FILTER(?prop IN {get_interesting_props()}).'
        ' ' +
        '?obj rdfs:label ?objLabel. ' +
        'FILTER(LANG(?objLabel) = "en"). ' +
        '}'
    )
    
    raw_res = return_sparql_query_results(wd_query)
    search_res = raw_res['results']['bindings']
    claims = {}
    for claim in search_res:
        propLabel = claim['propLabel']['value']
        objLabel = claim['objLabel']['value']
        if propLabel not in claims:
            claims[propLabel] = []
        claims[propLabel].append(objLabel)
    return claims

def query_details(id):
    data_local = query_details_local(id)

    app_name = data_local['AppName'][0].split(" ")[0]
    data_wd = query_details_wikidata(app_name)

    return (data_local, data_wd)
